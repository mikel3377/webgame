<?php
    
include('lib/applicationlib.php');
include('lib/pathfinding.php');


if(isset($_POST['action']) && isset($_POST['gameid']))
{    
    $gameid = mysql_real_escape_string( $_POST['gameid']);
    $s = GameDataService::GetInstance();
    $game = $s->GetGameByID( $gameid );
    
    $updateList=array();

    if($_POST['action']=="move" && isset( $_POST['unitid']) && isset($_POST['x']) && isset($_POST['y']))
    {        
        $unitid = mysql_real_escape_string( $_POST['unitid']);
        $x = mysql_real_escape_string( $_POST['x']);
        $y = mysql_real_escape_string( $_POST['y']);
        
        $newParams = array('x' => $x, 'y' => $y);
        HandleTargetedAbility( $unitid, 'move', $newParams, $game, $updateList);
        echo json_encode( array( 'status' => $updateList ) );
        return;
            
    }else if( $_POST['action']=="targeted ability" && isset( $_POST['abilityname'] ) && isset( $_POST['unitid'] ) && isset($_POST['targetx']) && isset($_POST['targety']) && isset($_POST['x']) && isset($_POST['y']))
    {
        $unitid = mysql_real_escape_string( $_POST['unitid']);
        $abilityname = mysql_real_escape_string( $_POST['abilityname']);
        $targetx = mysql_real_escape_string( $_POST['targetx']);
        $targety = mysql_real_escape_string( $_POST['targety']);
        $x = mysql_real_escape_string( $_POST['x']);
        $y = mysql_real_escape_string( $_POST['y']);
        
        if (UnitHasMoved($unitid, $x, $y)){
            $newParams = array('x' => $x, 'y' => $y);
            HandleTargetedAbility( $unitid, 'move', $newParams, $game, $updateList);
        }
        
        $newParams = array('x' => $targetx, 'y' => $targety);
        if (isset($_POST['param1']) ){
            $newParams['param1'] = $_POST['param1'];
        }
        
        HandleTargetedAbility( $unitid, $abilityname, $newParams, $game, $updateList);
        echo json_encode( array( 'status' => $updateList) );
        return;
    }
    else if(  $_POST['action']=="endturn" )
    {
        EndTurn($game, $updateList);
        echo json_encode( array( 'status' => $updateList ) );
        return;   
    }
    echo json_encode( array( 'status' => "Invalid action parameters") );
    return;
}

function HandleTargetedAbility( $unitid, $abilityname, $newParams, $game, &$updateList){
    $unitrow = mysql_fetch_array(GetThisUnit($unitid));
    $ability = UnitConfiguration::GetAbility($unitrow['type'],$abilityname);
    if ($ability){
        $ability->Apply($game, $unitrow, $newParams, $updateList);
    } else{
        die( json_encode( array( 'status' => 'unit ' . $unitid . ' doesnt have ability ' . $abilityname ) ) );
    }
}

function EndTurn( $game, &$updateList)
{
    $user = Application::GetCurrentUser();
    $gameid = $game->ID;
    $query = "SELECT ownerid FROM units WHERE gameid = $game->ID GROUP BY ownerid";
    $result = mysql_query($query) or die( json_encode( array( 'status' => mysql_error())));

    if( $game->ActiveUser != $user->ID )
    {
        die( json_encode( array( 'status' => "You can only end your own turn $game->ActiveUser $user->ID" ) ) );
    }
    
    $next = false;
    $first = true;
    $firstid = -1;
    //$status = "";
    while($row = mysql_fetch_array($result))
    {
        if( $first  )
        {
            $first =false;
            $firstid = $row['ownerid'];
        }
        if( $next )
        {
            $newid = $row['ownerid'];
            $query = "UPDATE games SET activeuser=$newid WHERE id = $game->ID";
            mysql_query($query) or die( mysql_error());

            $query = "INSERT INTO gameupdates (gameid, type, param1 ) VALUES ($game->ID, 3, $newid )";
            mysql_query($query) or die( array( 'status' => mysql_error()) );
            
            EndOfPlayerTurn($game->ActiveUser, $game, $updateList );
            return;
        }
        if( $row['ownerid'] == $game->ActiveUser )
            $next = true;
    }
    $newid = $firstid;
    $query = "UPDATE games SET activeuser=$newid WHERE id = $game->ID";
    mysql_query($query) or die( array( 'status' => mysql_error()) );
       
    $query = "INSERT INTO gameupdates (gameid, type, param1 ) VALUES ($gameid, 3, $newid )";
    mysql_query($query) or die( array( 'status' => mysql_error()) );
    
    EndOfPlayerTurn($game->ActiveUser, $game, $updateList );
}

function UnitHasMoved($id, $x, $y){
    $query = "SELECT x, y FROM units WHERE id = $id";
    $result = mysql_query($query) or die( json_encode( array( 'status' =>mysql_error())));
    $row = mysql_fetch_row($result);
    if ($x == $row[0] && $y == $row[1]){
        return FALSE;
    }
    return TRUE;
}

function EndOfPlayerTurn($id,$game, &$updateList){
    $MyUnits = GetMyUnits($id,$game->ID);
    $s = GameDataService::GetInstance();
    $map = $s->GetGameMap( $game );
    Harvest($id,$game,$map,$MyUnits);
    mysql_data_seek($MyUnits, 0);
    ApplyAuras($game, $MyUnits, $updateList);
    
    $query = "UPDATE units SET moves = moverange, actionused = FALSE WHERE units.gameid = $game->ID AND ownerid = $game->ActiveUser";
    mysql_query($query) or die( json_encode( array( 'status' => mysql_error())));
}

function Harvest($id,$game,$map,$units){
    $dr=0;
    while ($row = mysql_fetch_array($units)){
        $t = $map->Cells[$row['x']][$row['y']]->Type;
        $tile = TileConfiguration::GetByID( $t );
        $unit = UnitConfiguration::GetByID($row['type']);
        $dr=$dr+$tile->GatherSpeed*$unit->GatherSpeed;
    }
    $query="UPDATE usergames SET resources = resources + $dr WHERE gameid = $game->ID AND userid = $id";
    mysql_query( $query ) or die( array( 'status' => mysql_error()) );
}

function ApplyAuras($game, $units, &$updateList){
    while ($row = mysql_fetch_array($units)){
        $unit = UnitConfiguration::GetById($row['type']);
        $ability = $unit->GetAbility($row['type'],'lifeaura');
        if ($ability)
        {
            $newParams = 'herp';
            $ability->Apply($game, $row, $newParams, $updateList);
        }
    }
}
?>

<?php

//    echo json_encode( array( 'status' => $_SERVER['QUERY_STRING']) );
//    return;

include('lib/applicationlib.php');
use updatelib as ulib;


if(isset($_POST['gameid']) && isset($_POST['updateid']) && isset($_POST['updates']))
{
    $gameid = mysql_real_escape_string( $_POST['gameid']);
    $updateid = mysql_real_escape_string( $_POST['updateid']);

    $query = "SELECT * FROM gameupdates WHERE gameid = $gameid AND id > $updateid ORDER BY id ASC";
    $result = mysql_query($query);
   
    $updateList=array();
    
    while ($row = mysql_fetch_array($result))
    {
/*
        if ($row["type"]==1) //move
        {
            $updateList[] = new ulib\MoveUpdate($row["gameid"],(int)$row["id"],$row["param1"],$row["param2"],$row["param3"]);
        }
        
        if ($row["type"]==2) //attack
        {
            $updateList[] = new ulib\AttackUpdate($row["gameid"],(int)$row["id"],$row["param1"],$row["param2"],$row["param3"]);
        }
        
        if ($row["type"]==3) //turn update
        {
            $updateList[] = new ulib\TurnUpdate($row["gameid"],(int)$row["id"],$row["param1"]);
        }
*/
        $updateList[] = ulib\GetUpdateObject($row);
        
    }
    
    $response = array( "status" => "OK",
                       "updates" => $updateList );
    echo json_encode( $response );
    return;    
}
else if(isset($_POST['gameid']))
{

    $gameid = mysql_real_escape_string( $_POST['gameid']);
    $gameData = Application::$GameDataService->GetGameByID( $gameid );
    
    if( !isset( $_POST['updateid'] ) )
    {
        $response = array( "status" => "UPDATE", "data" => $gameData );
        echo json_encode( $response );
        return;        
    }
    
    $updateid = mysql_real_escape_string( $_POST['updateid']);
    
    if( (int)$updateid != $gameData->UpdateID )
    {        
        $response = array( "status" => "UPDATE", "data" => $gameData );
        echo json_encode( $response );
        return;
    }
    

    $response = array( "status" => "NO UPDATE" );
    echo json_encode( $response );
    return;
}
else
{
    echo json_encode( array( 'status' => 'gameid not set') );
    return;
}

echo json_encode( array( 'status' => 'invalid parameters') );
?>

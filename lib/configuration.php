<?php

    include('abilitylib.php');

    class Ability
    {
        public $Type;
        public $Name;
        public $Params;

        public function Ability( $t, $n, $p )
        {
            $this->Type = $t;
            $this->Name = $n;
            $this->Params = $p;
        }

        public static function Move()
        {
            return new Ability( 0, "move", null );           
        }

        public static function Attack()
        {
            return new Ability( 1, "attack", null);            
        }

        public static function SpawnUnit( $unitIDs )
        {
            return new Ability( 2, "spawnunit", $unitIDs );
        }

        public static function LifeAura( $params )
        {
            return new Ability(3, "lifeaura", $params);
        }
        
        public function Apply($game, $unitrow, $newParams, &$updateList)
        {
            $func = $this->Name . "Apply";
            $func($this, $game, $unitrow, $newParams, $updateList);
            return;
        }
    }
    
    class TileConfiguration
    {
        public $Type;
        public $Name;
        public $ImageSrc;
        public $Cost;
        public $DefenseBonus;
        public $GatherSpeed = 0;
        
        private static $config = NULL;
        
        public function TileConfiguration( $n, $image, $c, $def, $g )
        {
            $this->Name = $n;
            $this->ImageSrc = $image;
            $this->Cost = $c;
            $this->DefenseBonus = $def;
            $this->GatherSpeed=$g;
        }

        public static function GetByName( $name )
        {            
            $c = TileConfiguration::GetAll();
            foreach( $c as $config )
            {
                if( $config->Name == $name )
                    return $config;
            }
            return NULL;
        }
        
        public static function GetByID( $i )
        {
            $c = TileConfiguration::GetAll();
            return $c[ $i ];
        }
        
        
        public static function GetAll()
        {
            if( TileConfiguration::$config ) return TileConfiguration::$config; 
            
            $tiles = array();
            
            $tiles[] = new TileConfiguration( "Grass", "images/tiles/grasscell.png", 1, 0, 0 );
            $tiles[] = new TileConfiguration( "Tall Grass", "images/tiles/tallgrasscell.png", 2, 1, 0 );
            $tiles[] = new TileConfiguration( "Linoleum", "images/tiles/emptycell.png", 1, 0, 6 );
            $tiles[] = new TileConfiguration( "Water", "images/tiles/water.png", null, 0, 0 );
            $tiles[] = new TileConfiguration( "Ice", "images/tiles/ice.png", 0, 0, 0 );

            $i = 0;
            foreach( $tiles as $tile )
                $tile->Type = $i++;
            
            TileConfiguration::$config = $tiles;

            return TileConfiguration::$config;
        }         
    }
    
    class UnitConfiguration
    {
        private static $config = NULL;
     
        public static function GetByName( $name )
        {            
            $c = UnitConfiguration::GetAll();
            foreach( $c as $config )
            {
                if( $config->Name == $name )
                    return $config;
            }
            return NULL;
        }
        
        public static function GetByID( $i )
        {
            $c = UnitConfiguration::GetAll();
            return $c[ $i ];
        }
        
        public static function GetAbility($id,$name) //returns FALSE if unit doesn't have ability, else returns the ability
        {
            $unit = UnitConfiguration::GetByID($id);
            $abilities = $unit->Abilities;
            for($i = 0; $i < sizeof($abilities); ++$i)
            {
                if ($abilities[$i]->Name == $name)
                {
                    return $abilities[$i];
                }
            }
            return FALSE;
        }
        
        public static function GetAll()
        {
            if( UnitConfiguration::$config ) return UnitConfiguration::$config; 
            
            $units = array();
            
            $u = new UnitConfiguration();
            $u->Name = "Fortress";
            $u->ImageSrc = "images/tiles/fortress.png";
            $u->Defense = 0;
            $units[] = $u;
            $u->Abilities[] = Ability::SpawnUnit( array( 1, 2, 3, 4, 5, 6) );
            $u->GatherSpeed = 10;
            
            $u = new UnitConfiguration();
            $u->Name = "Scout";
            $u->ImageSrc = "images/units/scout.png";
            $u->AttackPower = 5;
            $u->AttackRange = 1;
            $u->Defense = 1;
            $u->MoveRange = 5;
            $u->ResourceCost = 200;
            $u->Abilities[] = Ability::Move();
            $u->Abilities[] = Ability::Attack();

            $units[] = $u;

            $u = new UnitConfiguration();
            $u->Name = "Beefcake";
            $u->ImageSrc = "images/units/beefcake.png";
            $u->MaxHP = 20;
            $u->AttackPower = 10;
            $u->AttackRange = 1;
            $u->Defense = 3;
            $u->MoveRange = 3;
            $u->ResourceCost = 300;
            $u->Abilities[] = Ability::Move();
            $u->Abilities[] = Ability::Attack();
            $units[] = $u;
            
            $u = new UnitConfiguration();
            $u->Name = "Ranger";
            $u->ImageSrc = "images/units/ranger.png";
            $u->AttackPower = 7;
            $u->AttackRange = 2;
            $u->Defense = 2;
            $u->MaxHP = 15;

            $u->MoveRange = 4;
            $u->ResourceCost = 250;
            $u->Abilities[] = Ability::Move();
            $u->Abilities[] = Ability::Attack();
            $units[] = $u;

            $u = new UnitConfiguration();
            $u->Name = "Worker";
            $u->ImageSrc = "images/units/worker.png";
            $u->AttackPower = 10;
            $u->AttackRange = 10;
            $u->Defense = 5;
            $u->MaxHP = 10;
            $u->GatherSpeed = 10;


            $u->MoveRange = 5;
            $u->ResourceCost = 250;
            $u->Abilities[] = Ability::Move();
            $u->Abilities[] = Ability::Attack();
            $u->Abilities[] = Ability::SpawnUnit( array( 0 ) );
            
            $units[] = $u;
            
            $u = new UnitConfiguration();
            $u->Name = "White mage";
            $u->ImageSrc = "images/units/whitemage.png";
            $u->AttackPower = 1;
            $u->AttackRange = 3;
            $u->Defense = 6;
            $u->MaxHP = 20;
            $u->GatherSpeed = 0;

            $u->MoveRange = 3;
            $u->ResourceCost = 400;
            $u->Abilities[] = Ability::Move();
            $u->Abilities[] = Ability::Attack();
            $u->Abilities[] = Ability::LifeAura(Array( 3,3 ));

            $units[] = $u;
            
            $u = new UnitConfiguration();
            $u->Name = "Black mage";
            $u->ImageSrc = "images/units/blackmage.png";
            $u->AttackPower = 1;
            $u->AttackRange = 3;
            $u->Defense = 6;
            $u->MaxHP = 20;
            $u->GatherSpeed = 0;

            $u->MoveRange = 3;
            $u->ResourceCost = 400;
            $u->Abilities[] = Ability::Move();
            $u->Abilities[] = Ability::Attack();
            $u->Abilities[] = Ability::LifeAura(Array( 3, -2 ));

            $units[] = $u;

            $i = 0;
            foreach( $units as $unit )
                $unit->Type = $i++;
            
            UnitConfiguration::$config = $units;
            return UnitConfiguration::$config;
        }
        
        public $Type;
        public $Name;
        public $ImageSrc;
        public $MaxHP = 10;
        public $AttackPower = 0;
        public $MoveRange = 0;
        public $Defense = 0;
        public $AttackRange = 0;
        public $ResourceCost = 250;
        public $GatherSpeed = 1;
        
        public $Abilities;
        
        public function UnitConfiguration()
        {
            $Abilities = array();
        }
    }
    
    class Configuration
    {
        public static function GetTileConfiguration()
        {
            return TileConfiguration::GetAll();
        }
        
        public static function GetUnitConfiguration()
        {
           return UnitConfiguration::GetAll();            
        }
    }
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

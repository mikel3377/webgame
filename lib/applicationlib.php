<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    include('mysqllib.php');

    
    class Application
    {
        public static $GameDataService = NULL;
        public static $GameData = NULL;
        
        public static function GetCurrentUser()
        {
            if( isset( $_SESSION['id'] ))
            {
                return self::$GameDataService->GetUserByID( (int)$_SESSION['id'] );
            }
            if( PageHelper::$RequireLogin )
                PageHelper::Redirect("login.php");
            return null;
        }
        
        public static function GetUserByName( $name )
        {
            return self::$GameDataService->GetUserByName( $name );
        }
        
        public static function CreateUser( $name, $pass )
        {
            MySql::CreateUser( $name, $pass );
        }
        
        public static function CheckPassword( $name, $pass )
        {
            $name = mysql_real_escape_string( $name );
            $query="SELECT pass, id FROM userpass WHERE user='$name'";
            $result = mysql_query($query) or die(mysql_error());
            $realpass = mysql_fetch_array($result);
            
            
            // TODO : uncommment password
            if (true){//$realpass[0] == pass_hash($pass)){
                $_SESSION['id'] = $realpass['id'];
                return TRUE;
            }
            else{
                return FALSE;
            }
        }
    }
    
    Application::$GameDataService = GameDataService::GetInstance();
    if( isset( $_GET['id']))
    {
        $game_id = mysql_real_escape_string($_GET['id']);
        Application::$GameData = Application::$GameDataService->GetGameByID( $game_id );
    }

    
    
    include('widgets/widgetlib.php');
    
    session_start();
        
    $rootDir = '/PhpProject1/';
    $uri = $_SERVER['REQUEST_URI'];
    $uri = substr( $uri, strlen($rootDir));
    $slashes = count( explode('/',$uri)) - 1;
    
    $prefix = "";
    for( $i = 0; $i < $slashes; $i++)
    {
        $prefix = $prefix."../";
    }
        
    class PageHelper
    {       
        public static $RequireLogin = true;
        private static $console = true;
        private static $javascripts = array( 
            "scripts/include/jquery-1.7.2.js",
            "scripts/include/jquery-ui-1.8.20.js",
            "scripts/include/angular.js",
            "scripts/lib.js");
        
        private static $stylesheets = array( 
            "styles/jqueryui/jquery-ui.css", 
            "styles/global.css",
            );
        
        private static $links = array( 
            'Home' => "index.php", 
            'Generate Data' => 'gamedatagen.php', 
            'Actions' => 'actiontest.php',
            'Challenge' => 'creategame.php');
        
        public static $PageTitle = "No page title set";

        public static $GETGameData = FALSE;
        
        // Data functions
        // Utility functions
        public static function AddScript( $script )
        {
            if( !in_array( $script, PageHelper::$javascripts ))
                PageHelper::$javascripts[] =  $script;
        }
        
        public static function AddScripts( $scripts )
        {
            foreach( $scripts as $script )
            {
                PageHelper::AddScript( $script );
            }
        }
        
        public static function AddStyle( $style )
        {
            if( !in_array( $style, PageHelper::$stylesheets ))
                PageHelper::$stylesheets[] =  $style;
        }
        
        public static function AddStyles( $styles )
        {
            foreach( $styles as $style )
            {
                PageHelper::AddScript( $style );
            }
        }
        
        public static function DisableConsole()
        {
            PageHelper::$console = false;
        }

        // Page rendering
        public static function Redirect( $url )
        {
           ?>
<!DOCTYPE html>
<html><head><script type="text/javascript"> window.location = "<?=$url?>";</script></head></html>
<?
            die();
        }
        
        public static function RenderError( $m )
        {
            global $message;
            $message = $m;
            PageHelper::Render( function()
            {
                global $message;
                echo $message;
            });
            return;
        }
        
        private static function RenderHead()
        {
            global $prefix;
    ?>
    <head>
    <title>PhpProject1 | <?= PageHelper::$PageTitle?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <?            
    foreach( PageHelper::$stylesheets as $style )
    {
        ?>
        <link rel="stylesheet" type="text/css" href="<?=$prefix.$style?>" />
        <?
    }
    foreach( PageHelper::$javascripts as $script )
    {
        ?>
        <script type="text/javascript" src="<?=$prefix.$script?>" ></script>
        <?
    }
    if( PageHelper::$GETGameData == TRUE )
    {
        $gameData = Application::$GameData;

        $user_id = Application::GetCurrentUser();
        if( ! $user_id ) 
            $user_id = 'null';

        $map = Application::$GameDataService->GetGameMap( $gameData );
        ?>
        <script type="text/javascript">
            var UserData = <? echo json_encode( $user_id ) ?>;
            var GameData = <?php echo json_encode( $gameData ) ?>;
            GameData.Map = <?php echo json_encode( $map ) ?>;
            var Data = GameData;
            var TileConfig = <?php echo json_encode( Configuration::GetTileConfiguration() ) ?>;
            var UnitConfig = <?php echo json_encode( Configuration::GetUnitConfiguration() ) ?>;
        </script>
        <?
    }
    ?>
        
        
    </head>
    <?          
        }
                
        public static function Render( $contentGen = null )
        {
            $user = Application::GetCurrentUser();
            global $c;

            if( PageHelper::$RequireLogin && !$user )
            {
                PageHelper::Redirect( "login.php" );
                return;
            }
            global $prefix;
            if( PageHelper::$GETGameData && !isset($_GET['id']))
            {
                PageHelper::Redirect( $_SERVER['REQUEST_URI']."?id=1");
            }
            $contentGen = ($contentGen != null ) ? $contentGen : function()
            {
                echo "NO CONTENT SET";
            }
            
?>
<!DOCTYPE html>
<html>
    <? PageHelper::RenderHead(); ?>     
    <body ng:app ng-click="rootClick($event)">
        <div id="page-title" class="ui-corner-all single-column-content">
            <h1><?= PageHelper::$PageTitle?></h1>
        </div>
        <? if( $user )
                {
        ?>
        <div class="ui-corner-all single-column-content">
            <?
                
                    foreach( PageHelper::$links as $label => $link )
                    {
                    ?>
            <div class="link-div"><a href="<?=$prefix.$link?>"><?=$label?></a></div>
                    <? 
                    }
            ?>            
            
            <div id="user-info" class="link-div"> 
                    <div style="height:1em;"><form method="post" name="logout" action="login.php">
                    <input type="submit" name="submit" value="Logout" />
                    </form></div>
                    <div style="margin-top:.2em;"><b>User:</b><?=$user->UserName?></div>                    
            </div>
           
            <div style="clear:both;"></div>
            <div></div>
        </div>
        <?}?>

        <div class="ui-corner-all single-column-content page-content">
                <? $contentGen(); ?>
        </div>
        
        <? if( PageHelper::$console ) {            
         } ?>
    </body>    
</html>
<?php            
        }       
    }   
    
    $trigrams = array("all",
"and",	
"are",
"but",
"ent",
"era" ,
"ere" ,
"eve",
"for",
"had",
"hat",	
"hen",	
"her",	
"hin",	
"his",	
"ing",	
"ion",	
"ith",	
"not",	
"ome",	
"oul",	
"our",	
"sho",	
"ted",
"ter",	
"tha",
"the",	
"thi",	
"tio",	
"uld",
        "fu",
"ver","was",
        "wit",
"you");
        
    
    class Rule
    {
        public $names;
        public $next;  
        
        public function Rule( $n, $next )
        {
            $this->names = $n;
            $this->next = $next;
        }
        
        static function FirstNames( $next )
        {
            return new Rule( array ( "Toast", "Patches", "Buddy", "Spider", "Peter","Le", "Eric","Ivan", "Evan","Lee","Nacho","Frank","Abe","Remi","Abraham","Ganny","Tim","Rames","Steve", "Jim", "Spencer", "Beef", "Pedro", "Shen", "Jimmy", "James", "Guy", "Captain Jack", "David", "Edward", "Eddy", "Mister", "Sir", "Professor", "Doctor", "Kevin", "Johnny", "Plank"), $next );
        }
        
        static function Trigrams( $next )
        {
            global $trigrams;
            return new Rule( $trigrams, $next );
        }
        
        static function LastStart1( $next )
        {
            return new Rule( array( "Hu", "Sprink", "Heim", "Dan", "Fru","Spa","Wel", "Smel", "Rustle", "Tag", "Dar", "Ramm","Rem","Mug", "Doo", "Pot", "Fus", "Loo", "Lan", "Rog", "Smert", "Derp", "Widdly", "Mar", "Edd", "Dik", "Ali", "Mor", "Qua", "Quar", "Plar", "Jen"), $next );            
        }
        
        static function LastMid1( $next )
        {
            return new Rule( array ('y', "stol", "ing", "ton", "ty", "ot", "ro", "sly", "ran", "mer", "ter", "oly", "o", "s", "ulu", "li", "dy", "er", "ling", "spider" ), $next );
        }
        
        static function LastEnd1( $next )
        {
            return new Rule( array ( "hoff", "dinger", "ton", "man", "son", "ston", "worth", "smith", "dale", "dah", "derp", "scuds", "cus", "cal", "zer", "vark", "inator", "smek", "go", "iels", "stein", "stolt", "han", "tits", "nuts" ), $next );
        }
        static function Space( $next )
        {
            return new Rule( array ( " " ), $next );
        }
        
        static function EmptyRule( $next )
        {
            return new Rule( array( "" ), $next );
        }
        
        static function Hyphen( $next )
        {
            return new Rule( array ( "-", "-", "-" ), $next );
        }
        
        static function BaseFirst ( $next )
        {
            return new Rule( array( "Super", "Jimmy", "Power", "Weapon", "Derp", "Summer", "Winter", "Village", "Town", "City", "Population", "Windy", "Blooming","Sunny","Ghost","King", "Giga", "Poop", "Sword", "Punching", "Battle", "Warrior", "Party"), $next);
        }
        
        static function BaseSecond( $next )
        {
            return new Rule( array(     " House", " Fortress", " Manor", " Tower", " City", " Hut", "mart", "geshayla", " Center", "topolis", "topia", "town", "ville", "dale", "fieldsdale", "fields", " Hills". ""), $next);
        }
        
        static function CityBattleStart( $next )
        {
            return new Rule( array("Battle of ", "Seige of ", "Battle of ", "Massacre of " ), $next);
        }
        
        static function CityBattleEnd( $next )
        {
            return new Rule( array(" War", " Battle", " Revolution", " Massacre" ), $next);
        }
        
        static function GameOf( $next )
        {
            return new Rule( array("Battle of ", "Seige of ", "War of ", "Battle of " ), $next);
        }
        
        static function GameOfEnd( $next )
        {
            return new Rule( array("" ), $next);
        }
        
        static function Year( $next )
        {
            return new Rule( array( "".rand(100,4000)), $next );
        }
    
        public static function GeneratorRule( $gen, $next )
        {
            return new Rule( array( $gen->Generate() ), $next );
        }
        
    }
    
    class NameGenerator
    {
        public $rule;
     
        public function NameGenerator()
        {
            
        }
        public static function StandardNameGenerator()
        {
            $gen = new NameGenerator();
            
            $gen->rule = Rule::FirstNames( array(
                Rule::Space( array (
                    Rule::LastStart1( array (
                        Rule::LastEnd1( null ),
                        Rule::Trigrams( array(
                            Rule::LastEnd1( null )
                        ) ),
                        Rule::Trigrams( null ),
                        Rule::LastMid1( array(
                            Rule::LastEnd1( null ),
                            Rule::LastMid1( array(
                                Rule::LastEnd1( null )
                            ))
                        ))
                    )),
                    Rule::Trigrams( array( 
                        Rule::Trigrams( null ),
                        Rule::Trigrams( array(
                            Rule::Hyphen(array(
                                Rule::Trigrams(null),
                                Rule::LastEnd1(null)
                            )),
                            Rule::Trigrams( null ),
                            Rule::LastEnd1( null ),
                            Rule::LastMid1( array(
                                Rule::LastEnd1( null ),
                                Rule::Trigrams( null )                    
                            ))                            
                        ))
                    ))
                ))
            ));      
            return $gen;
        }
        
        public static function BaseNameGenerator()
        {
            $gen = new NameGenerator();
            
            $gen->rule = Rule::BaseFirst( array(
                   Rule::BaseSecond( null )
                ) );      
            return $gen;
        }
        
        public static function BattleNameGenerator()
        {
            $baseName = NameGenerator::BaseNameGenerator();
                        
            $gen = new NameGenerator();
            
            $gen->rule = Rule::EmptyRule( array(
                Rule::CityBattleStart( array(
                    Rule::GeneratorRule( $baseName, null ),
                    Rule::GeneratorRule( $baseName, null ),
                    Rule::Year( null )
                ) ),
                Rule::GeneratorRule( $baseName, array(
                    Rule::CityBattleEnd( null )
                ))
            ) );      
            return $gen;
        }
        
        
        public function Generate()
        {
            $name = "";
            $r = $this->rule;            
            while($r != null )
            {
                $v = rand(0, count( $r->names) - 1);
                $name = $name.$r->names[$v];                
                $v = rand( 0, count( $r->next ) - 1);
                if( $r->next == null )
                    break;
                $r = $r->next[$v];
            }
            $space = false;
            
            for($i = 0, $s = strlen($name); $i < $s; $i++)
            {
                if( $space && substr_compare( $name, "of ", $i, 3) != 0)
                    $name[$i] = strtoupper( $name[$i]);
                $space = ( $name[$i] === ' ');
            }
            return $name;   
        }
    }
    
    function GenerateName()
    {
        $v = NameGenerator::StandardNameGenerator();
        return $v->Generate();
    }
    
    function GenerateBaseName()
    {
        $v = NameGenerator::BaseNameGenerator();
        return $v->Generate();
    }
?>
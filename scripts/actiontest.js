/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function gen_div( label, value )
{
    if( value === undefined ) value = "";
    var s = "<div><div class='field-label'><div style='margin-right:1em;margin-top:.1em;'>"+label+"</div></div><div class='field-label'><input name='"+label+"' type='text' value='"+value+"'></input></div><div class='field-value'><button class='remove-button'>Remove</button></div></div>"
    $("#field-list").append( s );
    $(".remove-button").click( function( eventData ) 
    { 
        $(this).parent().parent().remove();
    }).button();
    
}

$(document).ready(function(){
    //$form = $("#action-form").submit( function() { return false;});
  
    WebGame.Console.logLimit = 100;
  
    $("#move-preset-button").click( function () {
        $("#page-target").attr('value', 'action.php');
        $("#field-list").children().remove();
        gen_div( 'action', 'move' );
        gen_div( 'unitid', '1' );
        gen_div( 'x', '1' );
        gen_div( 'y', '1' );
        gen_div( 'gameid', '1' );
    }).click();


    $("#query-preset-button").click( function () {
        $("#page-target").attr('value', 'query.php');
        $("#field-list").children().remove();
        gen_div( 'gameid', '1' );
        gen_div( 'updateid', '1' );
        gen_div( 'updates', '1' );
    });

    $("#attack-preset-button").click( function () {
        $("#page-target").attr('value', 'action.php');
        $("#field-list").children().remove();
        gen_div( 'action', 'attack' );
        gen_div( 'unitid', '1' );
        gen_div( 'target', '2' );
        gen_div( 'x', '1' );
        gen_div( 'y', '1' );
        gen_div( 'gameid', '1' );
    });

    $("#submit-button").click( function ()
    {
        var url = $("#page-target").attr('value');
        
        WebGame.Console.writeln( "calling "+url+ "?"+$("#action-form").serialize()); 

        $.ajax({
        type : "POST",
        url : url,
        dataType : "json",
        data : $("#action-form").serialize(),
        success : function ( data ) {
            WebGame.Console.writeln( "response:" ); 
            WebGame.Console.printObject( data, true, '') ;
            
        },
        error : function( request, status, message )
        {
            WebGame.Console.writeln("Failed");
            WebGame.Console.writeln('  '+message );
            WebGame.Console.writeln('  '+request );
            WebGame.Console.writeln('  '+status );
        }
        });

    });
    
    $("#add-field-button").click( function() 
    {
        gen_div( $("#add-field-name").attr('value'));
        
    }).button();
});
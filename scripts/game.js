/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    
    
    //WebGame.Map = new Map(GameData.Width,GameData.Height);
    //Synchronize( GameData );

    /*$("#end-turn-button").click( function (){
        $.ajax({
        type : "POST",
        url : "action.php",
        dataType : "json",
        data : {action: 'endturn', id : GameData.ID },
        success : function ( response ) {
            WebGame.Console.printObject( response );
        } 
    });
    });*/

    var d = new WebGame.GameData( GameData );

    d.Map.applyToAllCells( function(x,y) { 

        
        var cell = this;
        this.toggle = false;

        this.$element = $("#cell_"+x+"_"+y);
        if( d.Map.Cells[x][y].Type == 0 )
            {
                this.$element.css('opacity', '0');
            }
        this.$element.click( function(){
            d.Map.Cells[x][y].applyToNeighbors( function(){
                if(this.toggle)
                    this.$element.css('opacity','.3');
                else
                    this.$element.css('opacity','.8');
                    this.toggle = !this.toggle;

            },6,false);

            WebGame.Console.writeln( x +" "+y);
            //cell.$element.css("opacity", '.5');
        });
    });
    
    d.Map.Cells[5][5].applyToNeighbors( function(){ this.$element.css('opacity','.3');},2,true);

});

// CODE BELOW HERE IS NEVER USED ----------------------------------

function Synchronize( gameData )
{
    $(".current-player-name").html(gameData.ActiveUserName);
    GameData.ActiveUserName = gameData.ActiveUserName;
    GameData.ActiveUser = gameData.ActiveUser;
    var unitList = gameData.Units;
    for( var i in unitList )
    {   
        var unit = WebGame.Map.units[ unitList[i].ID ];
        if( typeof unit !== 'object')
        {
            // unit doesnt exist: create it
            var unitData = unitList[i];
            unit = new Unit( unitData.ID );
            unit.OwnerID = unitData.OwnerID;
            unit.color = unitData.Color;
            unit.Moves = unitData.Moves;
            WebGame.Map.units[unitData.ID] = unit;
            WebGame.Map.cells[unitData.X][unitData.Y].addUnit( unit ); 

        }
        else
        {
            //update position/canmove of units if they've changed
            //var newCanMove = unit.canMove();
            if(true)// WebGame.Map.cells[unitList[i].X][unitList[i].Y].unit !== unit || unit.Moves != unitList[i].Moves)
            {
                unit.Moves = unitList[i].Moves
                unit.cell.removeUnit();
                WebGame.Map.cells[unitList[i].X][unitList[i].Y].addUnit( unit );
                unit.cell = WebGame.Map.cells[unitList[i].X][unitList[i].Y];
                unit.cell.click = Cell.prototype.click;
                unit.cell.element.removeClass("bright-cell");
            }                    
        }
    }
}

function QueueUpdate()
{
    setTimeout( "QueueUpdate();", 1000 )

    $.ajax({
        type : "POST",
        url : "query.php",
        dataType : "json",
        data : {query: 'game', gameid : GameData.ID },
        success : function ( response ) {
            Synchronize( response.data );
        } 
    });
}

function Unit( id )
{
    this.id = id;
    this.cell = null;
    this.color = "red";
}

Unit.prototype.canMove = function()
{
    return (this.OwnerID == UserData.ID) && (GameData.ActiveUser == UserData.ID ) && this.Moves > 0;    
}

Unit.prototype.getHtml = function()
{
    return "<div class='unit' style='background-color:"+this.color+";'></div>";
}

function Cell( xpos, ypos, map )
{
    var cell = this;
    this.map = map;
    this.x = xpos;
    this.y = ypos;
    this.neighbors = [];
    this.unit = null;

    this.element = $("#cell_"+xpos+"_"+ypos);
    this.element.click( function (){
        cell.click();
    });
}

Cell.prototype.click = function()
{

    this.map.applyToAllCells( function() { 
        this.element.removeClass("bright-cell");
        this.click = Cell.prototype.click;
    });  

    if( this.unit != null && this.unit.canMove() )
    {        

        var unitCell = this;
        this.applyToNeighbors( function() 
        {

            if( this.unit === null )
            {

                this.click = function() { 
                    unitCell.unit.Moves = 0;
                    this.addUnit( unitCell.unit );
                    unitCell.removeUnit();
                    unitCell.unit = null;
                    $.ajax({
                        type : "POST",
                        url : "action.php",
                        dataType : "json",
                        data : {action: 'move', id : this.unit.id, x : this.x, y : this.y},
                        success : function ( data ) {
                            WebGame.Console.printObject( data );
                        },
                        error : function ( data, status, message)
                        {
                            WebGame.Console.writeln( status );
                            WebGame.Console.writeln( message );
                        }
                    });
                    this.map.applyToAllCells( function() {this.element.removeClass("bright-cell");} );
                    this.click = Cell.prototype.click;
                };
                this.element.addClass("bright-cell");
            }
        }, this.unit.Moves );
    }
}

Cell.prototype.applyToNeighbors = function( fn, distance, costFn, first )
{
    if( first === undefined || first === true )
        this.map.applyToAllCells( function() {
            this.utility = -1;
        });  
        
    if( costFn === undefined )
    {
        costFn = function() { return 1;}
    }


    if( this.utility < 0 )
        fn.call( this );
    this.utility = distance;
    distance -= costFn( this );
    if( distance >= 0)
    {
        for( var i = 0; i < this.neighbors.length; i++ )
        {
            if( this.neighbors[i].utility < distance )
                this.neighbors[i].applyToNeighbors( fn, distance, costFn, false )
        }            
    }
}

Cell.prototype.addUnit = function( unit )
{
    this.element.html( unit.getHtml() );
    this.unit = unit;
    this.unit.cell = this;
    if( this.unit.canMove() )
    {
        this.element.append( "<div class='cell-highlight'></div>");
    }
}

Cell.prototype.removeUnit = function()
{
    this.element.html( "" );
    this.unit = null;
}

function Map( width, height )
{
    var i, j;
    this.Width = width;
    this.Height = height;
    this.units = [];
    
    //create cell
    this.cells = [];
    for( i = 0; i < width; i++ )
    {
        this.cells[i] = [];
        for( j = 0; j < height; j++ )
        {
            this.cells[i][j] = new Cell( i, j, this );
        }
    }
        
    function makeNeighbors( cell1, cell2 )
    {
        cell1.neighbors.push( cell2 );                       
        cell2.neighbors.push( cell1 );
    }

    //link neighbors
    for( i = 0; i < width; i++ )
    {
        for( j = 0; j < height; j++ )
        {
            if( j < height - 1 )
                makeNeighbors(this.cells[i][j],this.cells[i][j+1] );            
            if( i < width - 1 )
                makeNeighbors(this.cells[i][j],this.cells[i+1][j] );

            if( i % 2 == 0)
            {
                if( i < width - 1 && j < height - 1 ) 
                    makeNeighbors(this.cells[i][j],this.cells[i+1][j+1] );
            }
            else
            {
                if( i < width - 1 && j > 0 )
                    makeNeighbors(this.cells[i][j],this.cells[i+1][j-1] );         
            }
        }
    }
    
}

Map.prototype.applyToAllCells = function ( fn )
{
    var i, j;
    for( i = 0; i < GameData.Width; i++ )
    {
        for( j = 0; j < GameData.Height; j++ )
        {
                fn.call( this.cells[i][j] )
        }
    }
}
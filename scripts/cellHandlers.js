/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
    
    
    
    WebGame.Console.writeln("Cell handlers loaded..."); 
    
    var dataService = angular.injector(['ng']).get( 'dataService' );
    var animationManager = angular.injector(['ng']).get('animationManager');


    function AttackAnimation( attacker, defender )
    {
        WebGame.Console.writeln("ATTACKING..."); 

        var duration = 120;
        var anim = animationManager.NewAnimation();
        anim.update = function()
        {
            if( duration%30 == 0 ) WebGame.Console.writeln("..."); 
            this.Finished = (duration-- <= 0);
        };
        anim.onFinished = function()
        {
            WebGame.Console.writeln("Animation complete!"); 
            dataService.UpdateGameData();
        };
    }

    function MoveUnit( unit, onSuccess )
    {
        $.ajax({
            type : "POST",
            url : "action.php",
            dataType : "json",
            data : {action: 'move', unitid : unit.ID, x : unit.X, y : unit.Y, gameid : unit.GameID},
            success : function ( data ) {
                console.log(data);
                WebGame.Console.printObject( data );
                if( typeof onSuccess !== 'undefined')
                    onSuccess();
            },
            error : function ( data, status, message)
            {
                WebGame.Console.writeln( status );
                WebGame.Console.writeln( message );
            }
        });        
    }
    
    function SpawnUnit( t, xpos, ypos, spawner )
    {
//               isset( $_POST['type'] ) && isset( $_POST['owner'] ) && isset($_POST['x']) && isset($_POST['y']) )
        var game = dataService.GetGameData();
        WebGame.Console.printObject({action: 'targeted ability', abilityname: 'spawnunit', unitid: spawner.ID, x: spawner.X, y: spawner.Y, targetx: xpos, targety: ypos, gameid: game.ID, param1 : t});
        $.ajax({
            type : "POST",
            url : "action.php",
            dataType : "json",
            data : {action: 'targeted ability', abilityname: 'spawnunit', unitid: spawner.ID, x: spawner.X, y: spawner.Y, targetx: xpos, targety: ypos, gameid: game.ID, param1 : t},
            success : function ( data ) {
                console.log(data);               
                dataService.UpdateGameData();
            },
            error : function ( data, status, message)
            {

                WebGame.Console.writeln( status );
                WebGame.Console.writeln( message );
                WebGame.Console.printObject( data );
                dataService.UpdateGameData();

            }
        }); 
    }
    
    WebGame.Unit.prototype.Moving = false;

    WebGame.Map.prototype.Attack=function( attacker, target )
    {
        map = this;

        $.ajax({
            type : "POST",
            url : "action.php",
            dataType : "json",
            data : {action: 'targeted ability', abilityname: 'attack', unitid : attacker.ID, x : attacker.X, y : attacker.Y, targetx : target.X, targety: target.Y, gameid : attacker.GameID},
            success : function ( data ) {
                console.log(data);
                AttackAnimation( attacker, target );
                map.animationManager.addAttackAnimation(attacker,target,data.status[data.status.length-1].Damage );
            },
            error : function ( data, status, message)
            {
                WebGame.Console.writeln( status );
                WebGame.Console.writeln( message );
                AttackAnimation( attacker, target );
                map.animationManager.addAttackAnimation(attacker,target,data.status[data.status.length-1].Damage);

            }
        });  
    }

    WebGame.Unit.prototype.CanMove = function ()
    {
        //WebGame.Console.writeln( (this).Moves );
        //WebGame.Console.writeln( (this).OwnerID +", "+UserData.UserID );
        //WebGame.Console.writeln( (this).OwnerID +", "+this.Game.ActiveUser );
        if( this.OwnerID !== UserData.ID )
            return false;
        if( this.OwnerID !== this.Game.ActiveUser )
            return false;
        if( this.Moves <= 0 )
            return false;
        return true;
    }
    
    WebGame.Map.prototype.setAllClickHanlders = function ( fn )
    {
        this.applyToAllCells( function() { this.onClick = fn; });
    }

    WebGame.Map.prototype.resetClickHandlers = function( resetHighlight )
    {
        if( typeof resetHighlight === 'undefined' ) resetHighlight = true;
        this.applyToAllCells( function() { 
            if( resetHighlight ){
                this.Highlight = null;
                this.Highlight3 = null;
            }
                
            this.onClick = WebGame.Cell.prototype.onClick;
        });
    }
    
    WebGame.Map.prototype.GetPossibleTargets = function ( unit )
    {
        var list = [];
        //WebGame.Console.writeln("checkin targs");
        this.Cells[unit.X][unit.Y].applyToNeighbors( function (){
            
           if( this.Unit != null && this.Unit.OwnerID != unit.OwnerID )
                list.push( this.Unit );
        }, unit.AttackRange, function () {return 1;});

        return list;
    }
    
    WebGame.Cell.prototype.onMouseover = function( x, y ) {
        //WebGame.Console.writeln('on cell ' +  String(x) + ', ' + String(y));
        this.Highlight2 = WebGame.tileImages.Highlight2;
    }

    
    WebGame.Cell.prototype.onMouseout = function( x, y ) {
        //WebGame.Console.writeln('off cell ' +  String(x) + ', ' + String(y));
        this.Highlight2 = null;
    }

    function FindAbility( arr, name )
    {
        for( var a in arr )
        {
            if( arr[a].Name == name ) return arr[a];
        }
        return null;
    }

    function GenerateActions( unit, cell, tooltip )
    {
        var ability = UnitConfig[ unit.Type ].Abilities;
        var map = cell.GetMap();

        if( FindAbility( ability, 'attack') && !unit.ActionUsed )
        {
            var targets = map.GetPossibleTargets( unit );

            if( targets.length > 0 )
            {
                tooltip.AddButton("Attack", function()
                {
                    cell.applyToNeighbors( function (){
                        if (this.Unit != unit && (this.Unit == null || this.Unit.OwnerID != unit.OwnerID)){
                            this.Highlight3 = WebGame.tileImages.Highlight3;
                        }
                        if(this.Unit != null && this.Unit.OwnerID != unit.OwnerID )
                        {
                            var target = this.Unit;
                            this.onClick = function() 
                            { 
                                map.Attack( unit, target );
                                unit.ActionUsed = true;
                                unit.Moving = false;
                                map.resetClickHandlers();
                            };
                        }
                    }, unit.AttackRange, function () {return 1;});
                });
            }
        }    
        if( FindAbility( ability, 'spawnunit') && !unit.ActionUsed )
        {
            var list = FindAbility( ability, 'spawnunit').Params;
            for( var i = 0; i < list.length; i++ )
            {
                var u = UnitConfig[list[i]];
                if(unit.Game.GetActivePlayer().Resources < u.ResourceCost ) continue;
                var spawnClick = (function(){
                    var type = u.Type;
                    return function() {
                        cell.applyToNeighbors( function (){
                        if( this.Unit != null || TileConfig[this.Type].Cost == null ) return;
                        this.Highlight = WebGame.tileImages.Highlight;
                        var x = this.X;
                        var y = this.Y;
                        console.log(this);
                        this.onClick = function()
                        {
                            SpawnUnit( type, x, y, unit );
                            map.resetClickHandlers();
                        }                  
                        }, 1, function () {return 1;});
                    };
                })();
                tooltip.AddButton(  u.Name+" ("+u.ResourceCost+")", spawnClick );
            }
        }  
    }

    function MoveClick( unit, targetx, targety, movesleft )
    {
        var cell = this;
        var map = this.GetMap();
        map.resetClickHandlers();

        var prevmoves = unit.Moves;
        var prevx = unit.X;
        var prevy = unit.Y;
        unit.X = targetx;
        unit.Y = targety;
        unit.Moves = movesleft;

        function Cancel()
        {
            map.resetClickHandlers();
            unit.X = prevx;
            unit.Y = prevy;
            unit.Moves = prevmoves;
            
            //WebGame.Console.writeln("ACTION CANCELLED");
        }
        map.setAllClickHanlders( function () {
            Cancel();
            map.resetClickHandlers();
        })

        ToolTip.Clear();
        
        GenerateActions( unit, cell, ToolTip );
        
        ToolTip.AddButton( "End", function() 
        { 
            MoveUnit(unit );
            map.resetClickHandlers();
        });


        ToolTip.SetCancel( Cancel ).Show();

    }

    function ActiveUnitClick( unit, x, y )
    {
        var cell = this;
        var ability = UnitConfig[ unit.Type ].Abilities;
        
        if( FindAbility( ability, 'move') && unit.Moves > 0 )
        {

            this.applyWithMoveFunction( function( movesLeft )
            {
                if( this.Unit == null || this.Unit == unit )
                {                    
                    this.Highlight = WebGame.tileImages.Highlight;
                    this.onClick = function( movex, movey )
                    {
                        MoveClick.call(this, unit, movex, movey, movesLeft );
                        return;
                    }
                }
                            
            },unit.Moves, unit);
        }
        else if( ability.length > 0 )
        {
            ToolTip.Clear();
            GenerateActions( unit, cell, ToolTip );
            
            if( ToolTip.Size() > 0)
                ToolTip.Show();            
        }
        
    }
    
    function UnitClick( unit )
    {

    }

    WebGame.Cell.prototype.onClick = function( x, y )
    {
        //WebGame.Console.writeln( "onClick (prototype)");
        var map = this.GetMap();
        map.resetClickHandlers();

        if( this.Unit != null )
        {
            var game = this.Unit.Game;
            var unit = this.Unit;
            if( unit.OwnerID == game.ActiveUser && unit.OwnerID == UserData.ID )
            {
                ActiveUnitClick.call(this, unit, x, y );
            }
            else
            {
                UnitClick( unit, x, y );
            }
        }
        return;
    }
   
});

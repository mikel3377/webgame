$(document).ready(function() {
    WebGame.Console.printObject(MapData);
    WebGame.Console.printObject(GameData);
    WebGame.Console.printObject(UserData);
    var scale=0.7;
    var spacer=1;
    var canvas, context;
    var curX;
    var curY;
    curX=0;
    curY=0;
    var dragging;
    var button_down;
    button_down=false;
    dragging=false;
    myCells = new all_cells();
    myCells.gen_cells(spacer,GameData.Width,GameData.Height);

    init();

    function init () {
        // Find the canvas element.
        canvas = document.getElementById('imageView');
        if (!canvas) {
        alert('Error: I cannot find the canvas element!');
        return;
        }

        if (!canvas.getContext) {
        alert('Error: no canvas.getContext!');
        return;
        }

        // Get the 2D canvas context.
        context = canvas.getContext('2d');
        if (!context) {
        alert('Error: failed to getContext!');
        return;
        }
        // Event handlers
        canvas.addEventListener('mousedown',process_mousedown,false);
        canvas.addEventListener('mouseup',process_mouseup,false);
        canvas.addEventListener('mousemove',process_mousemove,false)
        canvas.addEventListener('mouseout',process_mouseout,false)
        canvas.onmousewheel = function(event){
            wheel(event);
            return false;
        };

        context.fillStyle='black';
        context.fillRect(0,0,canvas.width, canvas.height);

        myCells.add_units();
        myCells.draw_all();
    }

    function all_cells() { //grid will be (rf+1) rows 
        //and (cf+1) cols, hexes will be sidelength of len and spacer spaces them
        this.cells = [];
        this.xf=null;
        this.yf=null;
        this.clicked=null; //stores what cell has been clicked
        this.highlights=null; //stores what cells are highlighted
        this.plainColor='white';
        this.clickedColor='yellow';
        this.offsetX = 41;
        this.offsetY = 79;
        this.sidelen = 45;
        this.drag_scale=1;

        this.gen_cells = function (spacer, xf, yf) {
            if (spacer === undefined){
                spacer = 5;
            }
            if (xf === undefined){
                xf = 15;
            }
            if (yf === undefined){
                yf = 25;
            }
            this.xf=xf;
            this.yf=yf;
            len=this.sidelen;
            for (i=0;i< xf;i++){ //i is the x
                this.cells[i]=[];
                for (j=0;j< yf;j++){ //j is the y
                    if (i%2 == 0){
                        var hexX=(i * (len+len*Math.sin(30 * Math.PI / 180) + spacer) + this.offsetX)*scale
                        var hexY=(j * (2*len*Math.cos(30 * Math.PI / 180) +spacer) + this.offsetY)*scale
                        this.cells[i][j] = new hex(hexX,hexY,i,j,len*scale);
                    } else{
                        var hexX=(i * (len+len*Math.sin(30 * Math.PI / 180) + spacer) + this.offsetX)*scale
                        var hexY=(j * (2*len*Math.cos(30 * Math.PI / 180) +spacer) - len*Math.cos(30 * Math.PI / 180) - spacer/2 + this.offsetY)*scale
                        this.cells[i][j] = new hex(hexX,hexY,i,j,len*scale) 
                    }
                }
            }
            
            //establish neighbors
            
        }
        
        this.add_units = function(){
            for (i in GameData.Units){
                this.add_unit(GameData.Units[i])
            }
        }

        this.add_unit = function(unitData){
            this.cells[unitData.X][unitData.Y].add_unit(unitData);
        }

        this.wipe = function(){ //wipes the canvas
            context.save();
            // Use the identity matrix while clearing the canvas
            context.setTransform(1, 0, 0, 1, 0, 0);
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.fillStyle='black';
            context.fillRect(0,0,canvas.width, canvas.height);

            // Restore the transform
            context.restore();
        }
        this.draw_all = function(){ //draws all cells with their units
            for (var j = 0; j < this.yf; j++) {
                for (var i = 0; i < this.xf; i++){
                    this.cells[i][j].draw();
                }
            }
        }
        this.process_click = function(x,y){ //figures out which cell is clicked and calls cell_click
            for (var i = 0; i < this.cells.length; i++) {
                for (var j = 0; j < this.cells[i].length; j++){
                    if (this.cells[i][j].is_in_radius(x,y)){
                        this.cell_click(i,j);
                    }
                }
            } 
        }
        
        this.add_click_color = function(){
            if (this.clicked){
                var indi=this.clicked[0];
                var indj=this.clicked[1]; 
                this.cells[indi][indj].color = this.clickedColor;
                this.cells[indi][indj].draw();
            }
        }
        
        this.cell_click = function(i,j){ //does stuff when cell i,j is clicked
            if (! this.clicked){ //if we're clicking something and we don't have something clicked'
                if ((this.cells[i][j].unit) && (this.cells[i][j].unit.Moves > 0) && (this.cells[i][j].unit.OwnerID == GameData.ActiveUser) && (this.cells[i][j].unit.OwnerID == UserData.ID)){
                    WebGame.Console.printObject(this.cells[i][j].unit);
                    this.clicked=[i,j]; //store which cell was clicked
                    this.add_click_color();
                    
                }

            }

            else{
                var indi=this.clicked[0];
                var indj=this.clicked[1];
                if (i != indi || j != indj){
                    this.cells[indi][indj].color=this.plainColor;
                    this.cells[indi][indj].draw();
                    WebGame.Console.writeln('moving');
                    this.move(this.cells[indi][indj],i,j,this);
                    WebGame.Console.writeln('moved');
                    this.clicked=null;
                }
                else{
                    this.cells[indi][indj].color=this.plainColor;
                    this.cells[indi][indj].draw();
                    this.clicked=null;
                }
            }
        }
        
        this.move = function(oldCell,newi,newj,that){
            var newCell=this.cells[newi][newj];
            
            $.ajax({
                type : "POST",
                url : "action.php",
                dataType : "json",
                data : {action: 'move', id : oldCell.unit.ID, x : newi, y : newj},
                success : function ( data ) {
                    WebGame.Console.printObject( data );
                    newCell.unit=oldCell.unit;
                    newCell.unit.Moves=0;
                    oldCell.unit=null;
                    that.wipe();
                    that.draw_all();
                },
                error : function ( data, status, message)
                {
                    WebGame.Console.writeln( status );
                    WebGame.Console.writeln( message );
                }
            });
            
        }

        this.min_x = function() { //figures out the min x value of any cell
            var min;
            min = 999999
            for (var i = 0; i < this.cells.length; i++) {
                for (var j = 0; j<this.cells[i].length; j++){
                    if (this.cells[i][j].xpos < min){
                        min = this.cells[i][j].xpos;
                    }
                }
            }
            return min - scale*this.sidelen;
        }

        this.max_x = function() {
            var max;
            max = -999999
            for (var i = 0; i < this.cells.length; i++) {
                for (var j = 0; j<this.cells[i].length; j++){
                    if (this.cells[i][j].xpos > max){
                        max = this.cells[i][j].xpos;
                    }
                }
            }
            return max + scale*this.sidelen;
        }

        this.min_y = function() {
            var min;
            min = 999999
            for (var i = 0; i < this.cells.length; i++) {
                for (var j = 0; j<this.cells[i].length; j++){
                    if (this.cells[i][j].ypos < min){
                        min = this.cells[i][j].ypos;
                    }
                }
            }
            return min - scale*this.sidelen;
        }

        this.max_y = function() {
            var max;
            max = -999999
            for (var i = 0; i < this.cells.length; i++) {
                for (var j = 0; j<this.cells[i].length; j++){
                    if (this.cells[i][j].ypos > max){
                        max = this.cells[i][j].ypos;
                    }
                }
            }
            return max + scale*this.sidelen;
        }

        this.can_offset = function(dx,dy){ //determines if the offset can be done or whther it would push the view outside the map limits
            var can;
            can=true; //assume we can until we find we can't'
            if (dx!=0){
                if (dx > 0){
                    if (this.min_x()+ dx >= 0 && this.max_x() + dx >= canvas.width){
                        can=false;
                        //window.alert('minx>0')
                    }
                } else{
                    if (this.max_x() + dx <= canvas.width && this.min_x() + dx <= 0){
                        can=false;
                        //window.alert('maxx<width')
                    }
                }
            }

            if (dy !=0){
                if (dy>0){
                    if (this.min_y() + dy >=0 && this.max_y() + dy >= canvas.height){
                        can=false;
                        //window.alert('miny>0')
                    }
                } else {
                    if (this.max_y() +dy <=canvas.height && this.min_y() + dy <= 0){
                        can=false;
                        //window.alert('maxy<height')
                    }
                }
            }
            return can;

        }

        this.offset_cells = function(dx,dy){
            this.drag_scale = 1/scale;
            if (this.can_offset(dx,dy)){
                this.offsetX += dx*this.drag_scale;
                this.offsetY += dy*this.drag_scale;
                for (var i = 0; i < this.cells.length; i++) {
                    for (var j = 0; j<this.cells[i].length; j++){
                        this.cells[i][j].xpos += dx*this.drag_scale*scale;
                        this.cells[i][j].ypos += dy*this.drag_scale*scale;
                    }
                }
            }
        }
    }

    function hex(x0,y0,xind,yind,sidelen,color){ //x0,y0 is the center, len is a side length, color is the color, rind and cin are row and column indices
        this.xpos=x0;
        this.ypos=y0;
        this.xind=xind;
        this.yind=yind;
        this.len=sidelen;
        this.unit=null;
        var r=(sidelen * Math.cos(30 * Math.PI / 180));
        this.r2=r*r;
        this.neighbors=[];

        if (color === undefined){
                this.color='white';
            }
        this.show_coords = function(){
            WebGame.Console.writeln(this.xind);
            WebGame.Console.writeln(this.yind);
        }

        this.add_unit = function(unitData){
            this.unit=unitData;
        }

        this.draw = function(){
            this.draw_cell();
            this.draw_unit();
        }

        this.draw_cell = function (){ //draws on the current variable "context"
            context.fillStyle=this.color;
            context.beginPath();
            var x=this.xpos - 0.5 * sidelen;
            var y=this.ypos - sidelen * Math.cos(30 * Math.PI / 180);
            context.moveTo(x,y);
            x=x+sidelen;
            y=y;
            context.lineTo(x,y);
            x = x + sidelen * Math.sin(30 * Math.PI / 180);
            y = y + sidelen * Math.cos(30 * Math.PI / 180);
            context.lineTo(x,y);
            x = x - sidelen * Math.sin(30 * Math.PI / 180);
            y = y + sidelen * Math.cos(30 * Math.PI / 180);
            context.lineTo(x,y);
            x=x-sidelen;
            y=y;
            context.lineTo(x,y);
            x = x - sidelen * Math.sin(30 * Math.PI / 180);
            y = y - sidelen * Math.cos(30 * Math.PI / 180);
            context.lineTo(x,y);
            context.closePath();
            context.fill();
            //draw center
            //context.fillStyle='red';
            //context.fillRect(this.xpos-2,this.ypos-2,4,4);

        }
        this.draw_unit = function(){
            if (this.unit){
                MaxHP=this.unit.MaxHP;
                HP=this.unit.HP;
                //draw the unit
                context.fillStyle=this.unit.Color;
                context.fillRect(this.xpos-this.len/4,this.ypos-this.len/4,this.len/2,this.len/2);
                
                //draw the health bar
                
                //These parameters set the health bar size relative to cell size
                borderFactor=0.03;
                xScale=1.3;
                yScale=0.32;
                y0Scale=0.32;
                
                //This is the info for the black background
                outerX=this.xpos-this.len*xScale/2;
                outerY=this.ypos+this.len*y0Scale;
                outerWidth=this.len*xScale;
                outerHeight=this.len*yScale;
                
                //This is the info for the red inner part
                innerX = this.xpos - this.len * (xScale/2 - borderFactor);
                innerY = this.ypos+this.len*(y0Scale + borderFactor);
                innerWidth = this.len*(xScale - borderFactor*2)*HP/MaxHP;
                innerMax = this.len*(xScale - borderFactor*2);
                innerHeight = this.len*(yScale - borderFactor*2);
                
                //draw the outside black part
                context.fillStyle='black';
                context.fillRect(outerX,outerY,outerWidth,outerHeight);
                
                //draw the inner red part
                gradient = context.createLinearGradient(innerX,innerY,innerX,innerY+innerHeight);
                gradient.addColorStop(0,'rgb(255,0,0)');
                gradient.addColorStop(0.6,'rgb(255,180,180)');
                gradient.addColorStop(1,'rgb(255,0,0)');
                context.fillStyle=gradient;
                context.fillRect(innerX,innerY,innerWidth,innerHeight);
                //If HP is missing, draw white to fill the rest
                if (HP < MaxHP){
                    context.fillStyle='white';
                    context.fillRect(innerX+innerWidth+borderFactor*this.len, innerY, innerMax-innerWidth-borderFactor*this.len, innerHeight);
                }
            }
        }
        this.is_in_radius = function(x,y)  {
            var dx=(x-this.xpos)
            var dy=(y-this.ypos)
            if (dx*dx + dy*dy <= this.r2){
                return true
            }
        }
    }

    var newx, newy;

    function get_mouse_coords(ev){
        var x, y;

        // Get the mouse position relative to the canvas element.
        if (ev.layerX || ev.layerX == 0) { // Firefox
            x = ev.layerX;
            y = ev.layerY;
        } else if (ev.offsetX || ev.offsetX == 0) { // Opera
            x = ev.offsetX;
            y = ev.offsetY;
        }

        newx=x;
        newy=y;
    }

    function process_mousedown (ev) {
        button_down=true;
        get_mouse_coords(ev);
        var x,y;
        x=newx;
        y=newy;

        curX=x;
        curY=y;
    }

    function process_mouseup (ev) {

        get_mouse_coords(ev);
        var x,y;
        x=newx;
        y=newy;

        var dx, dy;
        dx=x-curX;
        dy=y-curY;
        if (dragging){
            myCells.wipe();
            myCells.offset_cells(dx,0);
            myCells.offset_cells(0,dy);
            myCells.draw_all();
        } else {
            if (button_down){
                myCells.process_click(x,y);
            }
        }
        dragging=false;
        button_down=false;
    }

    function process_mousemove(ev) {
        get_mouse_coords(ev);
        var x,y;
        x=newx;
        y=newy;
        //WebGame.Console.writeln(newx);
        //WebGame.Console.writeln(newy);
        if (! button_down){
            curX=x;
            curY=y;
        }

        var dx, dy;
        dx=x-curX;
        dy=y-curY;
        if (dragging || dx*dx + dy*dy > 200){
            dragging=true;
            myCells.wipe();
            myCells.offset_cells(dx,0);
            myCells.offset_cells(0,dy);
            myCells.draw_all();
            curX=x;
            curY=y;
        }
    }
    
    function process_mouseout(ev){
        dragging=false;
        button_down=false;
    }
    
    function wheel(ev){
        scaleMin = 0.4
        scaleMax = 3.0;
        dScale = scale * ev.wheelDelta / 1200
        if (scale + dScale >scaleMax){
            dScale = scaleMax - scale;
        } else if (scale + dScale < scaleMin){
            dScale = scaleMin - scale;
        }
        oldScale=scale;
        if (dScale != 0){
            x1=newx;
            y1=newy;
            minx = myCells.min_x();
            maxx = myCells.max_x();
            miny = myCells.min_y();
            maxy = myCells.max_y();
            if (x1 < minx) {
                x1=minx;
            }
            if (x1 > maxx) {
                x1=maxx;
                WebGame.Console.writeln('x over');
            }
            if (y1 < miny) {
                y1=miny;
            }
            if (y1 > maxy) {
                y1=maxy;
                WebGame.Console.writeln('y over');
            }
            
            x1=x1/scale - myCells.offsetX;
            y1=y1/scale - myCells.offsetY;
            
            scale += dScale;
            myCells.offsetX = (x1 + myCells.offsetX) * oldScale / scale - x1;
            myCells.offsetY = (y1 + myCells.offsetY) * oldScale / scale - y1;
        }
        
        
        //myCells.offsetX += scale * (canvas.width/2 - newx);
        //myCells.offsetY += scale * (canvas.height/2 - newy);
        //myCells.offsetX += dScale * (-1*newx);
        //myCells.offsetY += dScale * (-1*newy);
        
        
        myCells.wipe();
        myCells.gen_cells(spacer,GameData.Width,GameData.Height);
        myCells.add_units();
        myCells.add_click_color();
        myCells.draw_all();
    }

});
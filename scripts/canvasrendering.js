/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
    WebGame.Console.writeln("Canvas drawing library loaded!");
    var Console = WebGame.Console;
    var canvas, context;
    var clearColor = "#103510";

    //add game object rendering functions
    WebGame.Cell.prototype.render = function ( x, y )
    {
        context.drawImage( this.Image, x, y );        
    };
    
    function hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }
    
    function fillHexagon(x,y,unit){
        gradient=context.createRadialGradient(x+20,y+20,1,x+20,y+20,25);
        var color = hexToRgb( unit.Color );
        if (unit.Moves > 0){
            gradient.addColorStop(0.45,'rgba('+color.r+','+color.g+','+color.b+',0.05)');
            gradient.addColorStop(0.7,'rgba('+color.r+','+color.g+','+color.b+',0.4)');
            gradient.addColorStop(0.75,'rgba('+color.r+','+color.g+','+color.b+',0.9)');
            gradient.addColorStop(1.0,'rgba('+color.r+','+color.g+','+color.b+',0.9)');

        } else {
            context.fillStyle='rgba('+color.r+','+color.g+','+color.b+',0.6)';
            //gradient.addColorStop(0.0,'rgba(180,180,180,0.0)');
            gradient.addColorStop(0.60,'transparent');
            gradient.addColorStop(0.75,'rgba('+color.r+','+color.g+','+color.b+',0.3)');
            gradient.addColorStop(1.0,'rgba('+color.r+','+color.g+','+color.b+',0.5)');
        }
        context.fillStyle=gradient;
        context.beginPath();
        context.moveTo(x+6,y);
        context.lineTo(x+34,y);
        context.lineTo(x+44,y+20);
        context.lineTo(x+34,y+40);
        context.lineTo(x+6,y+40);
        context.lineTo(x-4.5,y+20);
        context.closePath();
        context.fill();
    }
    
    function movableUnitBorder(x,y){
        context.beginPath();
        context.moveTo(x+6,y);
        context.lineTo(x+33.8,y);
        context.lineTo(x+31.8,y+4);
        context.lineTo(x+8,y+4);
        context.stroke();
        context.fill();
        
        
        context.beginPath();
        context.moveTo(x+34,y);
        context.lineTo(x+44,y+20);
        context.lineTo(x+40,y+20);
        context.lineTo(x+32,y+4);
        context.fill();
        context.stroke();
        
        context.beginPath();
        context.moveTo(x+44,y+20);
        context.lineTo(x+34,y+39);
        context.lineTo(x+32,y+35);
        context.lineTo(x+40,y+20);
        context.fill();
        context.stroke();
        
        context.beginPath();
        context.moveTo(x+34,y+39);
        context.lineTo(x+6,y+39);
        context.lineTo(x+8,y+35);
        context.lineTo(x+32,y+35);
        context.fill();
        context.stroke();
        
        context.beginPath();
        context.moveTo(x+6,y+39);
        context.lineTo(x-4,y+20);
        context.lineTo(x,y+20);
        context.lineTo(x+8,y+35);
        context.fill();
        context.stroke();
        
        context.beginPath();
        context.moveTo(x-4,y+20);
        context.lineTo(x+6,y);
        context.lineTo(x+8,y+4);
        context.lineTo(x,y+20);
        context.fill();
        context.stroke();
    }
    
    function nonmovableUnitBorder(x,y){
        context.beginPath();
        context.moveTo(x+6,y);
        context.lineTo(x+34,y);
        context.lineTo(x+44,y+20);
        context.lineTo(x+34,y+39);
        context.lineTo(x+6,y+39);
        context.lineTo(x-4,y+20);
        context.closePath();
        context.stroke();
        
        context.strokeStyle='gray';
        context.beginPath();
        
        context.moveTo(x+8,y+4);
        context.lineTo(x+32,y+4);
        context.lineTo(x+40,y+20);
        context.lineTo(x+32,y+35);
        context.lineTo(x+8,y+35);
        context.lineTo(x,y+20)
        context.closePath();
        context.stroke();
    }
    
    function fillBorder(x,y,unit){
        var color = hexToRgb( unit.Color );
        if (unit.Moves > 0){
            context.fillStyle='rgba('+color.r+','+color.g+','+color.b+',0.9)';
            context.strokeStyle='black';
            //context.strokeStyle='rgba('+color.r+','+color.g+','+color.b+',0.9)';
            context.lineWidth=0.3;
            movableUnitBorder(x,y);
        } else {
            context.fillStyle='rgba('+color.r+','+color.g+','+color.b+',0.35)';
            context.strokeStyle='black';
            //context.strokeStyle='rgba('+color.r+','+color.g+','+color.b+',0.2)';
            context.lineWidth=0.3;
            movableUnitBorder(x,y);
        }
    }
    
    function fillRectangle(x,y,unit){
        gradient=context.createRadialGradient(x+20,y+20,1,x+20,y+20,20);
        var color = hexToRgb( unit.Color );
        if (unit.Moves > 0){
            gradient.addColorStop(0.45,'rgba('+color.r+','+color.g+','+color.b+',0.05)');
            gradient.addColorStop(0.7,'rgba('+color.r+','+color.g+','+color.b+',0.4)');
            gradient.addColorStop(0.8,'rgba('+color.r+','+color.g+','+color.b+',0.9)');
            gradient.addColorStop(1.0,'rgba('+color.r+','+color.g+','+color.b+',0.0)');

        } else {
            context.fillStyle='rgba('+color.r+','+color.g+','+color.b+',0.6)';
            gradient.addColorStop(0.0,'rgba(180,180,180,0.0)');
            gradient.addColorStop(0.50,'rgba(180,180,180,0.3)');
            gradient.addColorStop(0.68,'rgba('+color.r+','+color.g+','+color.b+',0.3)');
            gradient.addColorStop(1.0,'rgba('+color.r+','+color.g+','+color.b+',0.0)');
        }
        context.fillStyle=gradient;
        context.fillRect(x-10, y-5, 60, 50);
    }
    
    WebGame.Unit.prototype.render = function ( x, y )
    {
        //fillRectangle(x,y,this);
        //fillHexagon(x,y,this);
        fillBorder(x,y,this);
        context.drawImage(UnitConfig[this.Type].Image,x,y,40,40);
    };
    
    WebGame.Unit.prototype.renderhp = function ( x, y )
    {   
        context.fillStyle = 'white';
        context.fillRect(x,y-3,40,6);
        context.fillStyle = 'black';
        context.fillRect(x+0.5,y-2.5,39,5);
        if (this.HP > 0){
            gradient = context.createLinearGradient(x+0.5,y-2.5,x+0.5,y+2.5);
            gradient.addColorStop(0,'rgb(255,0,0)');
            gradient.addColorStop(0.2,'rgb(255,0,0)');
            gradient.addColorStop(0.6,'rgb(255,150,150)');
            gradient.addColorStop(1,'rgb(255,0,0)');
            context.fillStyle=gradient;
            context.fillRect(x+0.5,y-2.5,39*(this.HP/this.MaxHP),5);
        }
    };

    function renderCells( map, fn )
    {
        for( var y = 0; y < map.Height; y++ )
        {
            for( var i = 1; i >= 0; i--)
            {
                for( var x = 0; x+i < map.Width; x += 2 )
                {
                    fn.call( map.Cells[x+i][y], x+i, y );
                }
            }
        }
    }
    
    function wipeCanvas(){ //Mike added this
        context.save();
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.fillStyle = clearColor;
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.restore();
    }
    
    WebGame.Map.prototype.render = function( x, y ){
        var map = this;
        var mapx = x;
        var mapy = y;
        
        wipeCanvas(); //Mike added this
        
        renderCells( map, function( x, y ) {
            //if( x % 2 == 1 ) y -= .5; MIKE EDITED THIS OUT AND ADDED THE LINE BETOW - now the top edge and left edge line up with the canvas
            if( x % 2 == 0 ) y += .5;
            var xpos = mapx + x*map.TileX;
            var ypos = mapy + y*map.TileY;
            var unitx = xpos;
            var unity = ypos;

            x = mapx + x*map.TileX;
            y = mapy + y*map.TileY;
            xpos = x;
            ypos = y;
            if( this.Image.height > map.TileY )
                ypos -= (this.Image.height - map.TileY );
            if( this.Image.width > map.TileX )
                xpos -= (this.Image.width - map.TileX )/2;

            this.render(xpos, ypos);
            unitx=x;
            unity=y;
            if( this.Unit !== null)
            {
                this.Unit.render( x, y );
            }
            
            var width;
            var height;
            
            if (this.Highlight != null){
                height = this.Highlight.height;
                width=this.Highlight.width;
            } else if (this.Highlight2 != null){
                height = this.Highlight2.height;
                width=this.Highlight2.width;
            }else if (this.Highlight3 != null){
                height = this.Highlight3.height;
                width=this.Highlight3.width;
            }
            
            if (this.Highlight != null || this.Highlight2 != null || this.Hightlight3 != null)
                if( height > map.TileY )
                    y -= (height - map.TileY );
                if( width > map.TileX )
                    x -= (width - map.TileX )/2;
            
            if( this.Highlight != null )
            {
                context.drawImage( this.Highlight, x, y );
            }
            
            if( this.Highlight2 != null )
            {
                context.drawImage( this.Highlight2, x, y );
            }
            
            if( this.Highlight3 != null )
            {
                context.drawImage( this.Highlight3, x, y );
            }
            
            if( this.Unit !== null)
            {
                if (map.showHealthBars) {
                    this.Unit.renderhp( unitx, unity );
                }
            }
                
        });
    }
    
    WebGame.GameData.prototype.SetupRender = function() //call before rendering
    {
        //WebGame.Console.writeln("render setup");

        var map = this.Map;
        this.Map.applyToAllCells( function(){

            //this.UpdateImage();
            this.Unit = null;
        });
        for( var i in this.Units )
        {
            var unit = this.Units[i];
            if( unit.HP > 0 )
                map.Cells[unit.X][unit.Y].Unit = unit;
        }
    }
    
    WebGame.GameData.prototype.render = function( x, y )
    {
        this.SetupRender();
        for( var i = 0; i < this.RenderObjects.length; i++)
            this.RenderObjects[i].draw();
        this.Map.render( x, y );
    }
    
    function InitImages( onLoaded )
    {        
        var tileImages = {
                Highlight : "images/tiles/highlight.png",
                Highlight2 : "images/tiles/highlightblue.png",
                Highlight3 : "images/tiles/highlightred.png"
        };
                
        WebGame.tileImages = {};
        var imageCount = 0;
        for( var i = 0; i < TileConfig.length; i++)
        {
            imageCount++;            
            var tile = TileConfig[i];
            tile.Image = new Image();
            tile.Image.onload = function() { 
                if( (typeof onLoaded === 'function') && (--imageCount) == 0 ) onLoaded();
            };
            //WebGame.Console.writeln("Tile " + image + " : " + tileImages[image] );
            tile.Image.src = tile.ImageSrc;
        }
        
        for( i = 0; i < UnitConfig.length; i++)
        {
            imageCount++;            
            var unit = UnitConfig[i];
            unit.Image = new Image();
            unit.Image.onload = function() { 
                if( (typeof onLoaded === 'function') && (--imageCount) == 0 ) onLoaded();
            };
            unit.Image.src = unit.ImageSrc;
        }
        
        for( var image in tileImages )
        {       
            imageCount++;            
            WebGame.tileImages[ image ] = new Image();
            WebGame.tileImages[ image ].onload = function() { 
                if( (typeof onLoaded === 'function') && (--imageCount) == 0 ) onLoaded();
            };
            //WebGame.Console.writeln("Tile " + image + " : " + tileImages[image] );
            WebGame.tileImages[ image ].src = tileImages[image];
        }
    }

    WebGame.Cell.prototype.UpdateImage = function ()
    {
        var type = this.Type;
        this.Image = TileConfig[type].Image;
        return;         
    }

    WebGame.GameData.prototype.InitializeCanvas = function( canvaselement, onLoaded )
    {
        WebGame.Console.writeln("Initializing canvas...");
        var Data = this;
        this.RenderObjects = [];
        canvas = canvaselement;
        context = canvas.getContext('2d');
    
        //Initialize data
        var map = this.Map;
        map.TileX = 40;
        map.TileY = 40;
        map.showHealthBars=true;
       
        //load images
        InitImages( ImagesLoaded )
        
        function ImagesLoaded()
        {
            WebGame.Console.writeln( "Images loaded!");
                    
            Data.Map.applyToAllCells(function()
            {
                this.UpdateImage();                   
            });
            
            //context.clearRect(0,0, canvas.width, canvas.height); !Mike commented this out because render calls wipe() now
            Data.render(0,0);
            WebGame.Console.writeln( "Map rendered!");           
            if( typeof onLoaded !== 'undefined')
                onLoaded();
        }
    };
});
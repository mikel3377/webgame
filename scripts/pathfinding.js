/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
    
    WebGame.Console.writeln("Pathfinding library loaded!");

    function GetCellsInRange( cells, moves, costFn  )
    {        
        if( costFn === undefined )
        {
            var type = this.Type;
            costFn = function() {       
                return TileConfig[type].Cost; 
            };
        }

        if( this.utility < 0 )
        {
            cells.push( this );
        }
        if (moves <= this.utility){
            return;
        }
        this.utility = moves;

        if( moves > 0)
        {
            var neighbors = this.getNeighbors();
            for( var i = 0; i < neighbors.length; i++ )
            {
                var u = moves;
                var cost = costFn.call(neighbors[i]);
                if( typeof cost === 'number' )
                    GetCellsInRange.call( neighbors[i], cells, u - cost, costFn );//.applyToNeighbors( fn, moves, costFn, false )
            }            
        } 
    }
    
    WebGame.Cell.prototype.applyToNeighbors = function( fn, distance, costfn )
    {
        this.GetMap().applyToAllCells( function() { this.utility = -1;});

        var cells = [];
        if( costfn == false || typeof costfn === 'undefined')
            GetCellsInRange.call( this, cells, distance );
        else
            GetCellsInRange.call( this, cells, distance, costfn);
        
        for( var i = 0; i < cells.length; i++ )
            fn.call( cells[i], cells[i].utility );
    }


    WebGame.Cell.prototype.applyWithMoveFunction = function( fn, distance, unit )
    {
        this.GetMap().applyToAllCells( function() { 
            this.enemyNeighbor = false;
            var ns = this.getNeighbors();
            for( var i = 0; i < ns.length; i++ )
            {
                var n = ns[i];
                if( n.Unit != null && n.Unit.OwnerID != unit.OwnerID )
                    this.enemyNeighbor = true;
            }
            if( this.Unit != null && this.Unit.OwnerID != unit.OwnerID )
                this.enemyNeighbor = true;

            this.utility = -1;
        });

        var costfn = function()
        {
            var t = this.Type;
            if( this.Unit != null && this.Unit.OwnerID != unit.OwnerID )
                return null;
            if( TileConfig[t].Cost == null )
                return null;
            if( this.enemyNeighbor )
                return 999;
            return TileConfig[t].Cost;    
        }
        
        var cells = [];
        if( costfn == false || typeof costfn === 'undefined')
            GetCellsInRange.call( this, cells, distance );
        else
            GetCellsInRange.call( this, cells, distance, costfn);
        
        for( var i = 0; i < cells.length; i++ )
        {
            //if( TileConfig[cells[i].Type].Cost < 1000)
                fn.call( cells[i], cells[i].utility );
        }
    }
});
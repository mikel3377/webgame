$(document).ready(function(){
    WebGame.Console.writeln("Canvas events library loaded!");
    var Console = WebGame.Console;
    var canvas, context;
    var mouseData;
    var Data;
    
    //--ADD EVENT LISTENERS AND INITIALIZE STUFF------
    
    WebGame.GameData.prototype.CanvasEventsInit=function(canvaselement, onLoaded){
        WebGame.Console.writeln("Initializaing canvas events...");
        Data = this;
        canvas = canvaselement;
        context = canvas.getContext('2d');
        this.AddCanvasListeners();
        gameWidth=(this.Map.TileX)*(this.Map.Width);
        gameHeight=(this.Map.TileY)*(this.Map.Height+0.5);
        mouseData = new MouseData();
        Data.Map.visSettings = new VisualSettings(gameWidth,gameHeight,this.Map.TileX,this.Map.TileY,canvas.width,canvas.height);
        Data.Map.visSettings.addAvailableOffset(-1,-1);
        //visSettings.setScaleLimits()
    }
    
    WebGame.GameData.prototype.AddCanvasListeners = function()
    {
        WebGame.Console.writeln("Adding canvas listeners...");
        //make it so the canvas can't be highlighted
        
        canvas.onselectstart = function() {return false;};
        canvas.style.MozUserSelect = "none";
        canvas.style.KhtmlUserSelect = "none";
        canvas.unselectable = "on";
        
        //add mouse listeners
        
        canvas.addEventListener('mousedown',process_mousedown,false);
        canvas.addEventListener('mouseup',process_mouseup,false);
        canvas.addEventListener('mousemove',process_mousemove,false);
        canvas.addEventListener('mouseout',process_mouseout,false);
        canvas.addEventListener('mousein',process_mousein,false);
        canvas.onmousewheel = function(event){ //non-firefox mousewheel
            process_mousewheel(event);
            return false;
        };
        window.addEventListener('keydown',process_windowKeyDown,true);
        $('#map-canvas').bind('DOMMouseScroll', function(event) {
            process_mousewheel(event);
            return false;
        });
    }
    
    
    
    
    WebGame.Cell.prototype.onClick = function( x, y )
    {
        this.Type = (this.Type + 1)%4;
        this.UpdateImage();
        WebGame.Console.writeln(x+","+y+" (Type "+this.Type+")");
    }
    
    //--STORES MOUSE POSITION DATA, DRAGGING METHODS, ETC
    
    function MouseData(){
        this.curX=0;
        this.curY=0;
        this.lastDragX;
        this.lastDragY;
        this.buttonDown=false;
        this.dragging=false;
        this.dragTol=10;
        this.inCanvas=false;
        this.onX=-1;
        this.onY=-1;
        this.onMap=false;
        
        
        this.updateCoords = function(evt){ // Get the mouse position relative to the canvas element.
            //if (ev.layerX || ev.layerX == 0) { // Firefox
            //    x = ev.layerX;
            //    y = ev.layerY;
            //} else if (ev.offsetX || ev.offsetX == 0) { // Opera
            //    x = ev.offsetX;
            //    y = ev.offsetY;
            //}
            //x=evt.clientX - left + window.pageXOffset;
            //this.curX=x;
            //this.curY=y;
            
            var obj = canvas;
            var top = 0;
            var left = 0;
            while (obj && obj.tagName != 'BODY') {
                top += obj.offsetTop;
                left += obj.offsetLeft;
                obj = obj.offsetParent;
            }

            // return relative mouse position
            this.curX = evt.clientX - left + window.pageXOffset;
            this.curY = evt.clientY - top + window.pageYOffset;
            
            this.inCanvas = true;
        }
        
        this.updateDragCoords = function(ev){
            this.lastDragX = this.curX;
            this.lastDragY = this.curY;
        }
        
        this.dragIfNeeded = function(){
            if (this.buttonDown){
                dx=this.curX-this.lastDragX;
                dy=this.curY-this.lastDragY;
                if (dx*dx + dy*dy > this.dragTol){
                    this.dragging=true;
                    this.drag(dx/Data.Map.visSettings.curScale,dy/Data.Map.visSettings.curScale);
                }
            }
        }
        
        this.keyDrag=function(dx,dy){
            Data.Map.visSettings.addAvailableOffset(dx,dy);
            mouseData.processMouseover();
        }
        
        this.drag = function(dx,dy){
            this.lastDragX = this.curX;
            this.lastDragY = this.curY;
            Data.Map.visSettings.addAvailableOffset(dx,dy);
        }
        
        this.getIndices = function(){
            gameCoords=Data.Map.visSettings.canvasToGameCoords(this.curX,this.curY);
            var worldx = gameCoords[0];
            var worldy = gameCoords[1];
            var xc = worldx/Data.Map.TileX;
            var xindex = Math.floor(xc);
            var yc = ((xindex%2)? worldy : worldy - Data.Map.TileY/2 )/Data.Map.TileY;
            var yindex = Math.floor(yc);
            var xp=xc%1;
            var yp=yc%1;
            
            if (yp < -5/2*(xp-0.125)){
                yindex=yindex + ((xindex%2)?-1:0);
                xindex=xindex -1;
            } else if ((yp-1) > 5/2 * (xp-0.125)){
                yindex=yindex + ((xindex%2)?0:+1);
                xindex=xindex -1;
            } else if (yp < 5/2 * (xp - 0.875)) {
                yindex=yindex + ((xindex%2)?-1:0);
                xindex=xindex +1;
            } else if ((yp-1) > -5/2*(xp-0.875)){
                yindex=yindex + ((xindex%2)?0:+1);
                xindex=xindex +1;
            }
            
            return [xindex,yindex];
        }
        
        this.processMouseover = function() {
            indices = this.getIndices();
            xindex = indices[0];
            yindex = indices[1];
            
            if (xindex != this.onX || yindex != this.onY){
                if( 0 <= this.onX && this.onX < Data.Map.Width && 0 <= this.onY && this.onY < Data.Map.Height && this.onMap){
                    Data.Map.Cells[this.onX][this.onY].onMouseout(this.onX,this.onY);
                }
                this.onX=xindex;
                this.onY=yindex;
                if( 0 <= xindex && xindex < Data.Map.Width && 0 <= yindex && yindex < Data.Map.Height ){
                    this.onMap=true;
                    Data.Map.Cells[xindex][yindex].onMouseover(xindex,yindex);
                } else{
                    this.onMap=false;
                }
            }
        }
        
        this.processMouseout = function(){
            mouseData.buttonDown=false;
            mouseData.dragging=false;
            mouseData.inCanvas = false;
            if( 0 <= this.onX && this.onX < Data.Map.Width && 0 <= this.onY && this.onY < Data.Map.Height && this.onMap){
                Data.Map.Cells[this.onX][this.onY].onMouseout(this.onX,this.onY);
            }
            mouseData.onMap=false;
            mouseData.onX=-1;
            mouseData.onY=-1;
        }
        
        this.processClick = function(){
            //Data.Map.visSettings.test();
            var indices=this.getIndices();
            var xindex=indices[0];
            var yindex=indices[1];
            context.fillStyle = "rgb(200,0,0)";  
            if( 0 <= xindex && xindex < Data.Map.Width && 0 <= yindex && yindex < Data.Map.Height )
                Data.Map.Cells[xindex][yindex].onClick( xindex, yindex );
        }
    }
    
    //--STORES VISUALIZATION SETTINGS AND METHODS-----
    
    function VisualSettings(gameWidth, gameHeight, tileWidth,tileHeight, canvasWidth, canvasHeight){
        this.tileWidth=tileWidth;
        this.tileHeight=tileHeight;
        this.canvasWidth=canvasWidth;
        this.canvasHeight=canvasHeight;
        this.gameWidth=gameWidth;
        this.gameHeight=gameHeight;
        this.offsetX=0;
        this.offsetY=0;
        this.minScale=Math.min(canvasWidth/gameWidth,canvasHeight/gameHeight)*1.01;
        this.maxScale=7.0;
        this.curScale=this.minScale;
        context.scale(this.curScale,this.curScale);
        
        this.setScale = function(newScale){
            this.curScale=newScale;
        }
        
        this.updateScaleLimits = function(newMin,newMax){
            this.minScale=newMin;
            this.maxScale=newMax;
        }
        
        this.canvasToGameCoords = function(x,y){
            return [(x-this.offsetX)/this.curScale,(y-this.offsetY)/this.curScale];
        }
        
        this.gameToCanvasCoords = function(x,y){ //Not 100% sure if this is correct - haven't had to use it yet
            return [(x*this.curScale + this.offsetX), (y *this.curScale + this.offsetY)];
        }
        
        this.tileToGameCoords=function(x,y){
            y=(Math.floor(x)%2)?y:y+0.5;
            return [x*this.tileWidth, y*this.tileHeight];
        }
        
        this.tileToCanvasCoords = function(x,y){
            WebGame.Console.writeln(['tile coords',x,y]);
            var gameCoords=this.tileToGameCoords(x,y);
            WebGame.Console.writeln(['game coords',gameCoords[0],gameCoords[1]]);
            WebGame.Console.writeln(['scale',this.curScale,'offsets',this.offsetX,this.offsetY])
            return this.gameToCanvasCoords(gameCoords[0],gameCoords[1]);
        }
        
        this.scaleIfCan = function(scaleFactor){
            var oldScale=this.curScale;
            var newScale=this.curScale*scaleFactor;
            if ((newScale <= this.maxScale) && (newScale>=this.minScale)){
                var gameCoords=this.canvasToGameCoords(mouseData.curX,mouseData.curY);
                this.setScale(newScale);
                this.scale(scaleFactor);
                var multiplier=oldScale/newScale-1;
                this.addAvailableOffset(gameCoords[0]*multiplier, gameCoords[1]*multiplier);
                mouseData.processMouseover();
            } 
        }
        
        this.scale = function(scaleFactor){
            context.scale(scaleFactor, scaleFactor);
        }
        
        this.addOffset = function(dx,dy){ //offset is in current game coords (I think)
            this.offsetX +=dx*this.curScale;
            this.offsetY +=dy*this.curScale;
            context.translate(dx,dy);
        }
        
        this.addAvailableOffset = function(dx,dy){
            var canvasBR=this.canvasToGameCoords(this.canvasWidth,this.canvasHeight);
            var canvasTL=this.canvasToGameCoords(0,0);
            if (dx!=0){
                if (dx>0 && canvasTL[0]>=0){
                    dx=Math.min(dx,canvasTL[0])-0.1;
                } else if (dx<0 && canvasBR[0] <= this.gameWidth) {
                    dx=Math.max(dx,(canvasBR[0]-this.gameWidth))+0.1;
                } else{
                    dx=0
                }
                this.addOffset(dx,0);
            }
            
            if (dy!=0){
                if (dy>0 && canvasTL[1]>=0){
                    dy=Math.min(dy,canvasTL[1])-0.1;
                } else if (dy<0 && canvasBR[1] <= this.gameHeight) {
                    dy=Math.max(dy,(canvasBR[1]-this.gameHeight))+0.1;
                } else {
                    dy=0;
                }
                this.addOffset(0,dy);
            }
            
            //center the short axis if it's shorter than the canvas length
            var canvasBR=this.canvasToGameCoords(this.canvasWidth,this.canvasHeight);
            var canvasTL=this.canvasToGameCoords(0,0);
            if (canvasBR[0]-canvasTL[0] > this.gameWidth){
                this.addOffset((canvasBR[0]-canvasTL[0] -this.gameWidth)/2 - this.offsetX/this.curScale,0);
            }
            if (canvasBR[1]-canvasTL[1] > this.gameHeight){
                this.addOffset(0,(canvasBR[1]-canvasTL[1] -this.gameHeight)/2 - this.offsetY/this.curScale);
            }
            
            //push the map right or down or left or up if it should be
            var canvasBR=this.canvasToGameCoords(this.canvasWidth,this.canvasHeight);
            var canvasTL=this.canvasToGameCoords(0,0);
            if (canvasTL[0]>0 && canvasBR[0] > this.gameWidth){
                this.addOffset(canvasBR[0]-this.gameWidth,0)
            }
            if (canvasTL[1]>0 && canvasBR[1] > this.gameHeight){
                this.addOffset(0,canvasBR[1]-this.gameHeight)
            }
            if (canvasTL[0] < 0 && canvasBR[0] < this.gameWidth){
                this.addOffset(canvasTL[0], 0)
            }
            if (canvasTL[1] < 0 && canvasBR[1] < this.gameHeight){
                this.addOffset(0, canvasTL[1])
            }
        }
        
        this.test=function(){
            WebGame.Console.writeln('testing');
            context.setTransform(this.curScale,0,0,this.curScale,this.offsetX,this.offsetY);
        }
    }
    
    //--EVENT LISTENER CALLBACKS-----
    
    function process_mousedown(ev){
        mouseData.buttonDown=true;
        mouseData.updateDragCoords();
    }
    
    function process_mouseup(ev){
        if (mouseData.buttonDown && ! mouseData.dragging){
            mouseData.processClick();
        }
        mouseData.buttonDown=false;
        mouseData.dragging=false;
    }
    
    function process_mousemove(ev){
        mouseData.updateCoords(ev);
        mouseData.dragIfNeeded();
        mouseData.processMouseover();
    }
    
    function process_mouseout(ev){
        mouseData.processMouseout();
    }
    
    function process_mousein(ev){
        mouseData.inCanvas = true;
    }
    
    function process_mousewheel(ev){
        var delta;
        if ('wheelDelta' in ev){
            delta = ev.wheelDelta;
        } else{
            delta = -40 * ev.originalEvent.detail;
        }
        var scaleFactor=(delta>0)?1.25:0.8;
        Data.Map.visSettings.scaleIfCan(scaleFactor);
    }
    
    function process_windowKeyDown(ev){
        //WebGame.Console.writeln(ev.keyCode);
        if (ev.keyCode==17){
            Data.Map.showHealthBars= ! Data.Map.showHealthBars;
        }
        
        if (mouseData.inCanvas){
            process_canvasKeyDown(ev);
        }
    }
    
    function process_canvasKeyDown(ev){
        switch (ev.keyCode)
        {
        case 87: //w
           mouseData.keyDrag(0,200/Data.Map.visSettings.curScale);
           break;
        case 65: //a
           mouseData.keyDrag(200/Data.Map.visSettings.curScale,0);
            break;
        case 83: //s
            mouseData.keyDrag(0,-200/Data.Map.visSettings.curScale);
            break;
        case 68: //d
            mouseData.keyDrag(-200/Data.Map.visSettings.curScale,0);
            break;
        case 81: //q
            Data.Map.visSettings.scaleIfCan(0.8*0.8*0.8);
            break;
        case 69: //e
            Data.Map.visSettings.scaleIfCan(1.25*1.25*1.25);
            break;
        }
    }
    
});

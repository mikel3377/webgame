<?php

    class MessageWidget extends Widget
    {        
        private $id;
        
        private $gamedata;
        
        public function MessageWidget()
        {
            //if( self::$new_id == 0 )    
            $this->addDefaultDependency("messagewidget");
            $this->id = self::$new_id++;
            
            $this->gamedata = Application::$GameData;
            
            
        }
        
        public function Render()
        {
            ?>
            <div ng-controller="MessageWidget" class="MessageWidget" ng-init="Init( <?=$this->id?>)" >
                
                <div class="message-tabs" style="min-height: 20em;">
                    <ul>
                        <li><a href="#messagetab">Console</a></li>
                        <li><a href="#settings">Settings</a></li>
                        <li><a href="#actiontest">Action Test</a></li>
                    </ul>
                    
                    
                    <div id="messagetab" >
                        
                        <div style="position:relative;">
                            <div class="console-display content-fill-to-margin"></div>
                        </div>

                    </div>
                    
                    <div id="settings">
                        <div>
                            <?if( $this->gamedata ){?>

                            <select id="change-user">
                                <option value="Change User">Change User</option>
                                <?
                                foreach( $this->gamedata->Players as $player )
                                {
                                    echo "<option value='$player->UserName'>$player->UserName</option>";
                                }
                                ?>
                            </select>
                            <?}?>
                        </div>
                    </div>
                    
                    <div id="actiontest">
                        <div>
                            Action Test
                        </div>
                    </div>
                </div>
                
                <div id="buttons-top">
                    <div class="nav-button" ng-repeat="button in buttons" >
                        <a ng-click="button.Click()">{{ button.Label }}</a>
                    </div>
                    <div style="clear:both;"></div>
                </div>

                <div id="buttons-bottom"></div>

            </div>
            <?
        }
    }
    
?>

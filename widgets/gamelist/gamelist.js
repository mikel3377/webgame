/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
    var divs = $(".widget-gamelist .game-div");
    divs.hover(
        function() { $(this).addClass( 'ui-state-hover' );},
        function() { $(this).removeClass('ui-state-hover');}
    ).click(
        function() { window.location = $(this).find( 'a' ).attr( 'href' ); }
    );
});
